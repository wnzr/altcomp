# Altcomp #
## Description ##
Simple CLI utility that downloads JSON data of two ALTlinux branches(by default p10 and sisyphus)
and outputs following JSON structure:
>{"exclusive_to_branch1":  
     {"arch":
         {"packages": [...], "length": },
          ... 
     },  
"exclusive_to_branch2":  
     {"arch":
         {"packages": [...], "length": },
          ... 
     },  
"newer_versions_in_branch2":  
     {"arch":
         {"packages": [...], "length": },
          ... 
     },  
}
## Dependencies
requests  
rpmlib

## Usage ##
>python altcomp.py  

or make it executable with
>chmod +x altcomp.py

and run with
> ./altcomp.py

By default output goes to stdout, which isn't terribly useful without piping or redirection.  

## Options ##
_--output file_ or _-o file_ to specify output file  
_--branch1_ and _--branch2_ or _-b1_ and _-b2_ to specify branches for comparison  
_-n_ or _--no-cache_ to explicitly download branch data even if cached version exists

