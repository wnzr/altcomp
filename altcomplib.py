import json
import requests
from rpm import labelCompare as lc


def get_branch_json(url,cachedir,cached=True):
    filename = f"{cachedir}/{url.split('/')[-1]}.json"
    data = None
    if cached:
        try:
            with open(filename) as f:
                data = json.load(f)

        except (json.JSONDecodeError,FileNotFoundError):
            pass

    if not data:
        req = requests.get(url)
        req.raise_for_status()
        data = req.json()
        write_json(data, filename)

    return data


def group_pkg_by_arch(data):
    result = {}
    for el in data['packages']:
        result.setdefault(el['arch'],[]).append(el)
    return result

def pkg_diff(data1, data2):
    result = {}
    for arch in data1:
        nameset = {pkg["name"] for pkg in data2[arch]}
        temp = [pkg for pkg in data1[arch] if pkg["name"] not in nameset]
        result[arch] = {"packages":temp,"length":len(temp)}
    return result

def compare_versions(data1,data2):
    result = {}
    for arch in data1:
        temp = []
        nameset = {pkg["name"] for pkg in data1[arch]} & {pkg["name"] for pkg in data2[arch]}
        data1_verrel = {pkg["name"]:(str(pkg["epoch"]),pkg["version"], pkg["release"])
                        for pkg in data1[arch] if pkg["name"] in nameset}

        data2_verrel = {pkg["name"]:(str(pkg["epoch"]),pkg["version"], pkg["release"])
                        for pkg in data2[arch] if pkg["name"] in nameset}

        for pkg in data1[arch]:
            if pkg["name"] in data1_verrel:
                if lc((data1_verrel[pkg["name"]]),(data2_verrel[pkg["name"]])) == 1:
                    temp.append(pkg)
        result[arch] = {"packages":temp,"length":len(temp)}
    return result

def write_json(dct,filename):
    with open(filename,'w') as f:
        f.write(json.dumps(dct))
