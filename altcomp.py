#!/usr/bin/env python
import altcomplib as acl
import argparse,json
from pathlib import Path

cachedir = '/tmp/'
cached = True

url = 'https://rdb.altlinux.org/api/export/branch_binary_packages/'
branch1, branch2 = 'p10','sisyphus'
available_branches = ['sisyphus', 'p10', 'p9']

parser = argparse.ArgumentParser()
parser.add_argument("-o","--output", type=Path, help="set output file")
parser.add_argument("-b1","--branch1", type=str, choices=available_branches)
parser.add_argument("-b2","--branch2", type=str, choices=available_branches)
parser.add_argument("-n","--no-cache", action="store_true", help="ignore cached branches and download fresh data")
args = parser.parse_args()

if args.no_cache:
    cached = False

if args.branch1:
    branch1 = args.branch1

if args.branch2:
    branch2 = args.branch2

if branch1 == branch2:
    print('branch1 and branch2 set to same value')
    raise SystemExit

branch1_url, branch2_url = url + branch1, url + branch2

branch1_pkgs = acl.group_pkg_by_arch(acl.get_branch_json(branch1_url,cachedir,cached))
branch2_pkgs = acl.group_pkg_by_arch(acl.get_branch_json(branch2_url,cachedir,cached))

excl_to_branch1 = acl.pkg_diff(branch1_pkgs,branch2_pkgs)
excl_to_branch2 = acl.pkg_diff(branch2_pkgs,branch1_pkgs)
newer_in_branch2 = acl.compare_versions(branch2_pkgs,branch1_pkgs)

output_json = {f'exclusive_to_{branch1}':excl_to_branch1,
               f'exclusive_to_{branch2}':excl_to_branch2,
               f'newer_versions_in_{branch2}':newer_in_branch2}

if args.output:
    acl.write_json(output_json,args.output.absolute())
else:
    print(json.dumps(output_json))
